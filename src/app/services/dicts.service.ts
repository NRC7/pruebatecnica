import { Injectable } from "@angular/core";
import { Dict } from "../models/Dict";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class DictsService {
  dictsUrl: string = "http://168.232.165.184/prueba/dict";

  constructor(private http: HttpClient) {}

  getDicts(): Observable<Dict> {
    return this.http.get<Dict>(this.dictsUrl);
  }
}
