import { Injectable } from "@angular/core";
import { Numbers } from "../models/Numbers";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class NumbersService {
  numbersUrl: string = "http://168.232.165.184/prueba/array";

  constructor(private http: HttpClient) {}

  getNumbers(): Observable<Numbers> {
    return this.http.get<Numbers>(this.numbersUrl);
  }
}
