import { Component, OnInit } from "@angular/core";
import { Dict } from "../../models/Dict";
import { Data } from "../../models/Dict";
import { DictsService } from "../../services/dicts.service";

@Component({
  selector: "app-dicts",
  templateUrl: "./dicts.component.html",
  styleUrls: ["./dicts.component.css"]
})
export class DictsComponent implements OnInit {
  dicts: Dict;
  dictsArray: any[];
  dictsSuccess: boolean;
  paragraphArray: string[];
  countsArray: string[];
  sums: number[];

  constructor(private dictsService: DictsService) {}

  ngOnInit() {}

  // Get Dicts btn click event
  clickDicts() {
    this.dictsService.getDicts().subscribe(dicts => {
      // Endpoint result
      this.dicts = dicts;
      // Endpoint status
      this.dictsSuccess = dicts.success;
      // Endpoint data
      this.dictsArray = dicts.data;
      // Get array of paragraphs
      this.paragraphArray = this.getParagraphs(this.dictsArray);
      // Get letter reps for every paragraph
      this.countsArray = this.getCountArray(this.paragraphArray);
      //Get the sum of every number inside each paragraph
      this.sums = this.getSums(this.paragraphArray);
    });
  }

  getSums(array: string[]) {
    let numbers: number[] = [];
    for (let x = 0; x < array.length; x++) {
      let sum = this.getNumbers(array[x]);
      numbers.push(sum);
    }
    return numbers;
  }

  getNumbers(myString: string) {
    myString = myString.replace(/\D/g, "");
    let sum = 0;
    for (let z = 0; z < myString.length; z++) {
      sum = sum + parseInt(myString.charAt(z));
    }
    return sum;
  }

  getParagraphs(array: Data[]) {
    let paragraphs: string[] = [];
    for (let x = 0; x < array.length; x++) {
      const element = array[x].paragraph;
      paragraphs.push(element);
    }
    return paragraphs;
  }

  getCountArray(array: any[]) {
    let reps: any[] = [];
    for (let z = 0; z < array.length; z++) {
      reps.push(this.countLetters(array[z]));
    }
    return reps;
  }

  countLetters(myString: string) {
    const myLetterArray: string[] = [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "ñ",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z"
    ];
    let reps: any[] = [];
    for (let j = 0; j < myLetterArray.length; j++) {
      reps.push(myString.split(new RegExp(myLetterArray[j], "gi")).length - 1);
    }
    return reps;
  }
}
