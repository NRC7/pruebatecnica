import { Component, OnInit } from "@angular/core";
import { Numbers } from "../../models/Numbers";
import { NumbersService } from "../../services/numbers.service";

@Component({
  selector: "app-numbers",
  templateUrl: "./numbers.component.html",
  styleUrls: ["./numbers.component.css"]
})
export class NumbersComponent implements OnInit {
  numbers: Numbers;
  numbersArray: number[];
  numbersSucces: boolean;
  uniqueNumbersArray: number[];
  repetitions: number[];
  lastIndexs: number[];
  firstIndexs: number[];
  sortedNumbers: number[];

  constructor(private numbersService: NumbersService) {}

  ngOnInit() {}

  // Get Numbers btn click event
  clickNumbers() {
    // Number service
    this.numbersService.getNumbers().subscribe(numbers => {
      // Get response data
      this.numbers = numbers;
      this.numbersArray = numbers.data;
      console.log(this.numbersArray);
      this.numbersSucces = numbers.success;
      // Get unique elements array
      this.uniqueNumbersArray = Array.from(new Set(this.numbersArray));
      console.log(this.uniqueNumbersArray);
      // Get array of repetitions
      this.repetitions = this.countReps(
        this.numbersArray,
        this.uniqueNumbersArray
      );
      // Get array of last indices
      this.lastIndexs = this.getLastIndex(
        this.numbersArray,
        this.uniqueNumbersArray
      );
      // Get array of first indices
      this.firstIndexs = this.getFirstIndex(
        this.numbersArray,
        this.uniqueNumbersArray
      );
      this.sortedNumbers = this.sortArray(this.uniqueNumbersArray);
    });
  }

  // Count repetitions of a single value
  countNumbers(array: Array<any>, value: any) {
    let i = 0;
    array.forEach(element => {
      if (value === element) {
        i++;
      }
    });
    return i;
  }
  // Count repetitions of many values
  countReps(array1: Array<any>, array2: Array<any>) {
    let repArray: Array<any> = [];
    for (let j = 0; j < array2.length; j++) {
      repArray.push(this.countNumbers(array1, array2[j]));
    }
    return repArray;
  }

  getLastIndex(array1: Array<any>, array2: Array<any>) {
    let lastIndex: Array<any> = [];
    for (let j = 0; j < array2.length; j++) {
      let index = array1.lastIndexOf(array2[j]);
      lastIndex.push(index);
    }
    return lastIndex;
  }

  getFirstIndex(array1: Array<any>, array2: Array<any>) {
    let firstIndex: Array<any> = [];
    for (let j = 0; j < array2.length; j++) {
      let index = array1.indexOf(array2[j]);
      firstIndex.push(index);
    }
    return firstIndex;
  }

  sortArray(array: Array<number>): Array<number> {
    let myArray: Array<number> = array;
    let isSorting = false;
    while (!isSorting) {
      isSorting = !isSorting;
      for (let i = 1; i < myArray.length; i++) {
        if (myArray[i - 1] > myArray[i]) {
          isSorting = false;
          let element = myArray[i - 1];
          myArray[i - 1] = myArray[i];
          myArray[i] = element;
        }
      }
    }
    return myArray;
  }
}
