export interface Dict {
  success: boolean;
  data: Data[];
}

export interface Data {
  paragraph: string;
  number: number;
  hasCopyright: string;
}
